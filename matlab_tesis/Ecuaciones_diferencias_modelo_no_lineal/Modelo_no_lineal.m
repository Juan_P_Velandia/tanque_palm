%%%Ecuaciones de Diferencia de Modelo no lineal
clc
clear all
close all
%%
%Varibles
Theta = 0.349066; % 20 grados 
Cd = 0.6;
p = 62.43 ;%lbf/pie³
g = 32.16 ;% ft/s²
qmi = 1 ; % ft3/s
Ao = qmi / (Cd*sqrt(2*g)); %determinanos por el caudal que necesitamos 1ft3
L = 0.328084 ; %ft
k1 = 2*p*L*tan(Theta);
Ts = 0.1;
tic


% inicializacion de las se�ales
N = 150 ;
y = zeros (N,1);
y(N) = 0.1; 
u = zeros (N,1);
u(N)=0.63;
figure(1)
subplot(2,2,1)
l_y = line(nan,nan,'Color','r','LineWidth',2);
title('Plant')
xlabel('Time[s]')
subplot(2,2,2)
l_u = line(nan,nan,'Color','b','LineWidth',2); 
title('Imput')
xlabel('Time[s]')

t = linspace(0,Ts*N-1,N)'; 
k=0;
while 1
    if toc > Ts
        tic
%       Corrimiento de vectores la posicion final queda libre 
        u(1:end-1) = u(2:end);
        u(end) = u(end);
        if k > 15
            u(end)= 0.63;
            k=0;
        end
        k = k+1;
%         Modelo no lineal
        y(1:end-1) = y(2:end);
        y(end) = y(end-1)+Ts*((u(end)-Cd*Ao*sqrt(2*g*y(end-1)))/(k1*y(end-1)));
%       Modelo estimado mediante grafica
% 		y(end) = b*u(end-1)+c*y(end-1);
        set(l_y,'XData' , t , 'YData' , y); 
        set(l_u,'XData' , t , 'YData' , u);
        drawnow
    end   
end
