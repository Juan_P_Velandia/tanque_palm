%%Criterios de dise�o de controlador
% Condiciones de dise�o
s = tf('s');
os=10
ts=4
zita=-log(os/100)/sqrt(pi^2+log(os/100)^2)
sigma=4/ts
wn=sigma/zita
wd=wn*sqrt(1-(zita^2))
theta=acos(zita)*180/pi 
G_c = (wn^2)/(s^2+2*zita*wn*s+wn^2)