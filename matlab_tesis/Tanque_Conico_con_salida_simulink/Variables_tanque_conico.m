% Variables_Modelo_no_lineal
clc
clear all
close all

%%
%Varibles
Theta = 0.349066; % 20 grados 
Cd = 0.6;
p = 62.43 ;%lbf/pie³
g = 32.16 ;% ft/s²
Ao = 1 / (Cd*sqrt(2*g));
qmi = 3.53147 ; % ft3
L = 0.328084 ; %ft
k1 = 2*p*L*tan(Theta);
