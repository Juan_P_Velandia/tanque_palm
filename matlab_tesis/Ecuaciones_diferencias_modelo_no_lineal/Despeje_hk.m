clc
clear all 
close all
syms h_k  h_k1  Ts qmi 
Theta = 0.349066; % 20 grados 
Cd = 0.6;
p = 62.43 ;%lbf/pie³
g = 32.16 ;% ft/s²
qmi = 1
Ao = qmi / (Cd*sqrt(2*g));
L = 0.328084 ; %ft
k1 = 2*p*L*tan(Theta);
assume(h_k,'real')
assume(h_k1,'real')
assume(Ts,'real')
pretty((h_k - h_k1)*Ts == (qmi - Cd*Ao*sqrt(2*g*h_k))/(k1*h_k));

Theta = 0.349066; % 20 grados 




solve(((h_k - h_k1)*Ts == (qmi - Cd*Ao*sqrt(2*g*h_k))/(k1*h_k)),h_k)




