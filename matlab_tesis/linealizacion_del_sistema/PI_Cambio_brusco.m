%%%PID Dyscrete

clc
clear all
close all

%%
%Varibles
Theta = 0.349066; % 20 grados 
p = 62.43 ;%lbf/pie�
g = 32.16 ;% ft/s²
Cd = 0.6;
R = 0.019685/2 ; %D = 6.25 mm RADIO
Ao = pi*R^2;
k2 = Cd*Ao*sqrt(2*g);
L = 0.328084 ; %ft o 10 cm
k1 = 2*L*tan(Theta);
%%Balance Points 
h_bar = 0.4; %ft
qmi = p*Cd*Ao*sqrt(2*g*h_bar); % ft^3/s flujo volumetrico
Ts = 0.1;
tic
% Rango de referencia
rmin = 0.3;
rmax = 0.5;
% Variables Controlador PI
%   
%           5(s+0.049651)
%  C(s) = ------------------
%                 s
%         4.9443(s+0.39334)          bo.S +b1
% C(s) = -------------------        ----------
%                 s                  a0.S + a1
a0 = 1;
a1 = 0;
b0 = 1.2;
b1 = 0.049651*4.9443;
% a0 = 1;
% a1 = 0;
% b0 = 4.9334;
% b1 = 4.9334*0.39334;
% Variables auxiliares controlador
T = 0.1;
k_t = 2/T;
aux_1 = (b0*k_t+b1)/(a0*k_t+a1);
aux_2 = (-b0*k_t+b1)/(a0*k_t+a1);
aux_3 = (-a0*k_t+a1)/(a0*k_t+a1);
% inicializacion de las se�ales
N = 1500;
y = zeros (N,1);
y(N) = 0.1; %iniciar el nivel del tanque
u = zeros (N,1);
u(N)=0;
r = zeros (N,1);
r(N) = 0.7;
e = zeros (N,1);
e(N) = 0;

figure(1)
subplot(2,2,1)
l_y = line(nan,nan,'Color','r','LineWidth',2);
title('Plant')
xlabel('Time[s]')
grid on

subplot(2,2,2)
l_u = line(nan,nan,'Color','b','LineWidth',2); 
title('Imput of Water Pump')
ylabel('qmi [ft^2/s]')
xlabel('Time[s]')
grid on
subplot(2,2,1)
l_r = line(nan,nan,'Color','b','LineWidth',2); 
% title(['Control Smith Ecuaciones de Diferencia'])
% legend('Flujo','Location','SouthEast')
% xlabel('iteraciones') 
% ylabel('Caudal [L/s]')
title('Respuesta del sistema ante un cambio de referencia brusco')
ylabel('Nivel [ft]')
xlabel('Tiempo [s]')
legend('Planta','Referencia','Location','SouthEast')
grid on
b = 0.1063
c = 0.8937

t = linspace(0,Ts*N-1,N)'; 
k=0;
while 1
    if toc > Ts
        tic
%       reference
        r(1:end-1) = r(2:end);
        r(end) = r(end);
        if k > 500
            r(end)= round(rmin+rand(1,1)*(rmax-rmin),1);
            k=0;
            
        end
        k = k+1;
        
%         Calculate error 
        e(1:end-1) = e(2:end);
        e(end) = e(end);
        e(end) = r(end) - y(end);%Reference - output
        
%         controller
        u(1:end-1) = u(2:end);
        u(end) = u(end);
%         u(end) = 4.9443 * e(end) -(0.9607 * 4.9443)* e(end - 1) + u (end
%         - 1); %Filtro ZoH Controlador
        u(end) = (aux_1 * e(end) + aux_2* e(end - 1) -aux_3* u (end - 1));%c2d Tustin

%         Model no-lineal
        y(1:end-1) = y(2:end);
%         noise = 0.01*randn;
        noise = 0;
%         noise = 0;
        if y(end) > 0 
            y(end) = noise +( y(end-1)+Ts*(((u(end))/p -k2*(y(end-1)))/(k1*y(end-1))));
        else %condition to get out the system from zero
%             y(end) = b*u(end-1)+c*y(end-1);%Model Lineal
            y(end) = 0.01;%Model Lineal
        end
        set(l_r,'XData' , t , 'YData' , r); 
            set(l_y,'XData' , t , 'YData' , y); 
        set(l_u,'XData' , t , 'YData' , u);
        drawnow
    end   
end
