var clientId = "ws" + Math.random();

// Create a client instance
var client = new Paho.MQTT.Client("34.237.156.128", 1883, clientId);



// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;

// connect the client
client.connect({onSuccess:onConnect});

// prueba de comentario
// called when the client connects
function onConnect() {
  // Once a connection has been made, make a subscription and send a message.
  console.log("Conectado Mqtt-WebSocket");
  //client.subscribe("in");
  //client.subscribe("out_controller");
  //client.subscribe("level");
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("onConnectionLost:"+responseObject.errorMessage);
  }
}

// called when a message arrives
function onMessageArrived(message) {
  console.log( message.destinationName +" : "+message.payloadString);
  if(message.destinationName == "in"){
    // sincronizacion del elemento ID con respescto al de la pagina HTML
    document.getElementById("in").textContent = message.payloadString;
  }
  if(message.destinationName == "out_controller"){
    // sincronizacion del elemento ID con respescto al de la pagina HTML
    document.getElementById("ValorB").textContent = message.payloadString;
  }
  if(message.destinationName == "level"){
    // sincronizacion del elemento ID con respescto al de la pagina HTML
    document.getElementById("ValorC").textContent = message.payloadString;
  }
}