% Variables_Modelo_no_lineal
clc
clear all
close all
%%Como se Guarda 
% save PID.mat PID
load('data_no_lineal_model.mat');%%Planta
h_t = ans;
time = 0:(10./(size(h_t))):10;
%%
%Varibles
%Varibles
Theta = 0.349066; % 20 grados 
p = 62.43 ;%lbf/pie�
g = 32.16 ;% ft/s²
Cd = 0.6;
R = 0.019685/2 ; %D = 6.25 mm RADIO
Ao = pi*R^2;
k2 = Cd*Ao*sqrt(2*g);
qmi=0.001464;% para 1ft de altura
qmi=0.0009262;% para 0.4ft de altura
L = 0.328084 ; %ft o 10 cm
k1 = 2*L*tan(Theta);
%%Balance Points 
h_bar = 0.4; %ft
% qmi = Cd*Ao*sqrt(2*g*h_bar); % ft^3/s flujo masico
qmi = p*Cd*Ao*sqrt(2*g*h_bar); % ft^3/s flujo volumetrico
% A = ((-(Ao)*(sqrt(g))*sqrt(h_bar)*Cd) -(sqrt(2))*(qmi -(sqrt(2))*(Ao*Cd)*sqrt(g*h_bar)))/(sqrt(2)*k1*h_bar^2);
% A = [(qmi-k2*sqrt(h_bar))/(k1*h_bar^2)];
A = [((-k2*k1*h_bar)/(2*sqrt(h_bar))-(k1*(qmi-k2*sqrt(h_bar))))/((k1*h_bar)^2)]
B=(1/(k1*h_bar));
C = 1;
D = 0;
G = ss(A,B,C,D)
[num,den] = ss2tf(A,B,C,D);
Gf = tf(num,den);
y = qmi*step(Gf,time);
figure('Color',[1 1 1])
plot(time,y,'b','LineWidth',2)
grid on
hold on
plot((time(1:(end-1)))',h_t,'k','LineWidth',2)
% legend('Linealizado','Planta','Location','SouthEast')
% title('Linealizaci�n vs Planta')
xlabel('Tiempo[s]') 
ylabel('Altura Nivel de Agua [ft]') 
% figure
% qmi_s =qmi*ones(1,10)
% plot(qmi_s)
% title('Caudal de entrada')
% legend('Caudal de entrada','Location','SouthEast')
% ylabel('Caudal[ft^3/s]') 
% xlabel('tiempo[s]') 
% ylim([0 0.1])
