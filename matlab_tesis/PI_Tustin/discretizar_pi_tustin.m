%%Funcion de Transferencia del controlador
%         4.9443(s+0.39334)
% C(s) = -------------------
%                 s
clc
clear all
close all
num = [4.9443 4.9443*0.39334];
den = [1 0] ;
G = tf(num,den);
Ts = 0.5; %sampling interval
% Par�metros de entrada Planta , Tiempo de muestreo y m�todo
Gd = c2d(G,Ts,'tustin');