// Create a client instance

var clientId = "ws" + Math.random();
// client = new Paho.MQTT.Client("34.75.204.239", 1883, clientId);
client = new Paho.MQTT.Client("34.75.204.239", 9001, clientId);

// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;

// connect the client
client.connect({onSuccess:onConnect});
nivel_grafico = 0;


// called when the client connects
function onConnect() {
  // Once a connection has been made, make a subscription and send a message.
  console.log("mensaje de Satisfacción estas conectado");
  client.subscribe("in");
  client.subscribe("out_controller");
  client.subscribe("level");
  client.subscribe("R");
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    console.log("onConnectionLost:"+responseObject.errorMessage);
  }
}

// called when a message arrives
function onMessageArrived(message) {
  console.log(message.destinationName +": "+message.payloadString);
  if(message.destinationName == "in")
  {
    document.getElementById("ValorA").textContent = message.payloadString ;
  }
  if(message.destinationName == "out_controller")
  {
    document.getElementById("ValorB").textContent = message.payloadString ;
  }
  if(message.destinationName == "level")
  {
    document.getElementById("ValorC").textContent = message.payloadString ;
    nivel_grafico = parseFloat(message.payloadString);
  }
  if(message.destinationName == "R")
  {
    document.getElementById("ValorD").textContent = message.payloadString ;
  } 
}