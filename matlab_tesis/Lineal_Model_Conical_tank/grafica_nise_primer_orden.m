% pag 166 nise funcion de transferencia mediante grafica
clc
clear all
close all
a = 1/8.9;
num = [0 a];
den = [1 a] ;
G = tf(num,den);
Ts = 0.5; %sampling interval
Gd = c2d(G,Ts);
%%Compare the continuos and discrete step responses
step(G, 'b',Gd,'r')
title('Step Response')
xlabel('Time[s]') 
ylabel('Height[ft]') 
