% Variables_Modelo_no_lineal
clc
clear all
close all
%%Como se Guarda 
% save PID.mat PID
load('PID.mat')%%Parametros del Controlador
%%
%Varibles
Theta = 0.349066; % 20 grados 
Cd = 0.6;
p = 62.43 ;%lbf/pie³
g = 32.16 ;% ft/s²
Ao = 1 / (Cd*sqrt(2*g));
% qmi = 3.53147 ; % ft3
L = 0.328084 ; %ft
k1 = 2*p*L*tan(Theta);


%%Balance Points 
h_bar = 0.4; %ft
qmi = Cd*Ao*sqrt(2*g*h_bar)
% h_bar = qmi^2/(2*g*(Cd^2)*(Ao)^2);
A = ((-(Ao)*(sqrt(g))*sqrt(h_bar)*Cd) -(sqrt(2))*(qmi -(sqrt(2))*(Ao*Cd)*sqrt(g*h_bar)))/(sqrt(2)*k1*h_bar^2);
B=(1/(k1*h_bar));
C = 1;
D = 0;
G = ss(A,B,C,D)
%No Valido
[num,den] = ss2tf(A,B,C,D);
G = tf(num,den);
step(G)
%Discretizando el modelo PID generado por el toolbox
Ts = 0.1; %sampling interval o tiempo de muestreo 
PI_Discontinous = c2d(PID,Ts)%PI_DISCRETIZADO
Plant_D = c2d(G,Ts)
