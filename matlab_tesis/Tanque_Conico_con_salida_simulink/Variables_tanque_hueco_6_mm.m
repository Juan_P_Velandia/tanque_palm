% Variables_Modelo_no_lineal
clc
clear all
close all

%%
%Varibles
Theta = 0.349066; % 20 grados 
p = 62.43 ;%lbf/pie�
g = 32.16 ;% ft/s²
Cd = 0.6;
R = 0.019685/2 ; %D = 6.25 mm RADIO
Ao = pi*R^2;
k2 = p*Cd*Ao*sqrt(2*g);
qmi=0.001464;% para 1ft de altura
qmi=0.0009262;% para 0.4ft de altura
L = 0.328084 ; %ft o 10 cm
k1 = 2*L*p*tan(Theta);
h_bar = 0.01;
