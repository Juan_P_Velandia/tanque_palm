%%%PID Dyscrete
clc
clear all
close all
%%
%Varibles
Theta = 0.349066; % 20 grados 
Cd = 0.6;
p = 62.43 ;%lbf/pie³
g = 32.16 ;% ft/s²
qmi = 1 ; % ft3/s
Ao = qmi / (Cd*sqrt(2*g)); %determinanos por el caudal que necesitamos 1ft3
L = 0.328084 ; %ft
k1 = 2*p*L*tan(Theta);
Ts = 0.1;
tic
% Rango de referencia
rmin = 0.1;
rmax = 0.8;
% inicializacion de las se�ales
N = 150 ;
y = zeros (N,1);
y(N) = 0.1; %iniciar el nivel del tanque
u = zeros (N,1);
u(N)=0;
r = zeros (N,1);
r(N) = 0.8;
e = zeros (N,1);
e(N) = 0;
figure(1)
subplot(2,2,1)
l_y = line(nan,nan,'Color','r','LineWidth',2);
title('Plant')
xlabel('Time[s]')
subplot(2,2,2)
l_u = line(nan,nan,'Color','b','LineWidth',2); 
title('Imput')
xlabel('Time[s]')
subplot(2,2,1)
l_r = line(nan,nan,'Color','b','LineWidth',2); 
title('Reference[BLUE] and Plant[RED]')
xlabel('Time[s]')

b = 0.1063
c = 0.8937

t = linspace(0,Ts*N-1,N)'; 
k=0;
while 1
    if toc > Ts
        tic
%       reference
        r(1:end-1) = r(2:end);
        r(end) = r(end);
        if k > 100
%             r(end)= round(rmin+rand(1,1)*(rmax-rmin),1);
            r(end) = 0.1 ; 
            k=0;
            
        end
        k = k+1;
        
%         Calculate error 
        e(1:end-1) = e(2:end);
        e(end) = e(end);
        e(end) = r(end) - y(end);%Reference - output
        
%         controller
        u(1:end-1) = u(2:end);
        u(end) = u(end);
        u(end) = 4.9443 * e(end) -(0.9607 * 4.9443)* e(end - 1) + u (end - 1);

%         Model no-lineal
        y(1:end-1) = y(2:end);
        if y(end) > 0 
            y(end) = y(end-1)+Ts*((u(end)-Cd*Ao*sqrt(2*g*y(end-1)))/(k1*y(end-1)));
        else
            y(end) = b*u(end-1)+c*y(end-1);
        end
        set(l_r,'XData' , t , 'YData' , r); 
        set(l_y,'XData' , t , 'YData' , y); 
        set(l_u,'XData' , t , 'YData' , u);
        drawnow
    end   
end
