clc
close all
clear all
tic
Ts = 0.5;
b = 0.05463;
c = 0.9454;

% inicializacion de se�ales
N = 70 ;
y = zeros (N,1);
u = zeros (N,1);
figure(1)
subplot(2,2,1)
l_y = line(nan,nan,'Color','r','LineWidth',2);
title('Plant')
xlabel('Time[s]')
subplot(2,2,2)
l_u = line(nan,nan,'Color','b','LineWidth',2); 
title('Imput')
xlabel('Time[s]')

t = linspace(0,Ts*N-1,N)'; 
k=0;
while 1
    if toc > Ts
        tic
%       Corrimiento de vectores la posicion final queda libre 
        u(1:end-1) = u(2:end);
        u(end) = u(end);
        if k > 15
            u(end)= 1;
            k=0;
        end
        k = k+1;
        y(1:end-1) = y(2:end);
        %y(end) = -a1*y(end-1)-a0*y(end-2)+b1*u(end-1)+b0*u(end-2);
		y(end) = b*u(end-1)+c*y(end-1);
        set(l_y,'XData' , t , 'YData' , y)
        set(l_u,'XData' , t , 'YData' , u)
        drawnow
    end   
end